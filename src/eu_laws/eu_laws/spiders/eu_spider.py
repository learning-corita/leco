import scrapy
from eu_laws.items import Article

class EuSpider(scrapy.Spider):
	name = 'eu-articles'
	urls_dict = {
			'https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32008R0340':'REACH' ,
			'https://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1531231211865&uri=CELEX:32017R0852':'Mercury',
			'https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32019L0904':'Plastics',
			'https://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1601561123103&uri=CELEX:32015L0720':'Plastic bags',
			'https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A01994L0062-20180704':'Shipment of waste',
		}

	def start_requests(self):
		urls_list = list(self.urls_dict.keys())
		tag = getattr(self, 'tag', None)
		if tag is not None:
			urls_list = [url for url, topic in self.urls_dict.items() if topic==tag]
		for url in urls_list:
			yield scrapy.Request(url, self.parse)

	def parse(self, response):
		for cnt, article in enumerate(response.xpath("//p[@class='ti-art' or @class='title-article-norm']"), start=1):
			code = 'eu_article'
			number = article.xpath('text()').get()
			title = article.xpath("following-sibling::p[@class='sti-art' or @class='stitle-article-norm']").xpath("text()").get()
			chapter =  article.xpath("(preceding-sibling::p[@class='ti-section-1'])[last()]").xpath("text()").get() + \
					   article.xpath("(preceding-sibling::p[@class='ti-section-2'])[last()]").xpath("text()").get()
			content = '\n'.join(article.xpath("following-sibling::p[@class='normal' or @class='norm' and count(preceding-sibling::p[@class='ti-art' or @class='title-article-norm'])=$cnt]",cnt=cnt).xpath("text()").getall())
			text_in_tables= '\n'.join(article.xpath("following-sibling::table[count(preceding-sibling::p[@class='ti-art' or @class='title-article-norm'])=$cnt]//p[@class='normal']", cnt=cnt).xpath("text()").getall())
			tags =  ['EU', self.urls_dict[response.url]]
			article = Article (
			code = code,
			chapter = chapter,
			title = title,
			content = content + '\n' + text_in_tables,
			tags = tags)
			yield article
			
			"""yield {
			'tags': ['EU', self.urls_dict[response.url]],
			'number': article.xpath('text()').get(),
			'title': article.xpath("following-sibling::p[@class='sti-art' or @class='stitle-article-norm']").xpath("text()").get(),
			'chapter_number': article.xpath("(preceding-sibling::p[@class='ti-section-1'])[last()]").xpath("text()").get(),
			'chapter_name': article.xpath("(preceding-sibling::p[@class='ti-section-2'])[last()]").xpath("text()").get(),
			'text': article.xpath("following-sibling::p[@class='normal' or @class='norm' and count(preceding-sibling::p[@class='ti-art' or @class='title-article-norm'])=$cnt]",cnt=cnt).xpath("text()").getall(),
			'text_in_tables': article.xpath("following-sibling::table[count(preceding-sibling::p[@class='ti-art' or @class='title-article-norm'])=$cnt]//p[@class='normal']", cnt=cnt).xpath("text()").getall()
            }"""
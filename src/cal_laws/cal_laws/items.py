# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
    
class Article(scrapy.Item):
    # define the fields for your item here like:
    code = scrapy.Field()
    division = scrapy.Field()
    part = scrapy.Field()
    chapter = scrapy.Field()
    number = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()
    tags = scrapy.Field()
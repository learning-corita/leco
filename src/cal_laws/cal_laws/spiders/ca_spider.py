import scrapy
from cal_laws.items import Article

class CalSpider(scrapy.Spider):
	name = 'ca-articles'
	start_urls = [
		'https://leginfo.legislature.ca.gov/faces/codes_displayexpandedbranch.xhtml?tocCode=PRC&division=30.&title=&part=&chapter=&article='
	]
	
	#Step 1
	def parse(self, response):
		for article_url in response.xpath("//div[contains(text(),'ARTICLE')]//ancestor::a/@href"):
			yield response.follow(article_url, callback=self.parse_article_pages)

	#Step 2
	def parse_article_pages(self, response):
		code = response.xpath("//h3/b").xpath("text()").get()
		division = response.xpath("//h4/b[contains(text(), 'DIVISION')]").xpath("text()").get()
		part = response.xpath("//h4/b[contains(text(), 'PART')]").xpath("text()").get()
		chapter = response.xpath("//h4/b[contains(text(), 'CHAPTER')]").xpath("text()").get()
		title = response.xpath("//h5/b[contains(text(), 'ARTICLE')]").xpath("text()").get()
		content = '\n'.join(response.xpath("//p").xpath("text()").getall())
		tags =  ['CA', 'Waste Management']
		article = Article (
		code = code,
		division = division,
		part = part,
		chapter = chapter,
		title = title,
		content = content,
		tags = tags)
		
		return article

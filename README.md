# LECO

This is a project to create SEO for data related to recycling world. It contains 2 different services:

1. You can include a source of data to be explored (legal document, dataset, news and similar). Further details are going to be added as project makes progress. 

2. Search for all (integrated into local dataset) related laws and regulations about topic related to recycling world, Basic idea behind is be able to make paths of articles related to a particular item to be recycled (i.e batteries). I will like to include some raspberry pi (want to keep fresh my knowledge about it :) + AWS services to handle distributed operation of the system. Also, to obtain and do first transformations, data science knowledge is also included. 

3. Future plans are including the creation of some text tutorials for proper treatment of some waste electronic elements refering to the impact of each treatment.

This all be headed to reach a deliverable to promote Circular Economy ideas into the WEEE. 

Most of documentation for specific sections of project would be saved into documents in PDF. Some of them will be in spanish.

## Installation

### Bare metal 
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install leco. 

All requirements will be included in requirements.txt

```bash
pip install -r requirements.txt
```

###Docker image
Instructions will be added soon. Kubernetes deployment will be included as well.

## Usage
Use of scrapy

```bash
scrapy crawl eu-articles -O articles.json -a tag=Mercury
```
## Download image from vagrant
Port verification 

vagrant port

```bash
$ scp -P 22 buildroot-2021.02.1/output/images/sdcard.img user@192.168.20.24:/home/user/Downloads 
```

## Test
TBD

##RPI
Get ip of raspberry

```bash
sudo nmap -sn 192.168.20.0/24
```
Update file in sdcard /etc/network/interfaces
https://raspberrypi.stackexchange.com/questions/10251/prepare-sd-card-for-wifi-on-headless-pi

Create /wpa_supplicant/wpa_supplicant.conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
network={
    ssid="YOUR_NETWORK_NAME"
    psk="YOUR_PASSWORD"
    key_mgmt=WPA-PSK
}
 use  wpa_passphrase SSID password to get some of the content of that file


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)